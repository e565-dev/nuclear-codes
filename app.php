<?php

global $locator;
global $party;

const SELECTOR = 5;

$locator = __DIR__ . '/data/partijos.csv';
$party = trim(file_get_contents($locator));

try {
    $csv = str_getcsv(file_get_contents(__DIR__ . '/data/' . $party));
} catch (\Throwable $th) {
    throw $th;
}

$formattedCsv = explode("\n", $csv[0]);
$selectiveCandidates = array_rand($formattedCsv, SELECTOR);

foreach ($selectiveCandidates as $key => $value) {
    echo ($value + 1) . "\n";
}
